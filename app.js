$(function() {
	//sample data for the table
	var SORT_ORDER = {
			"ASC": "asc",
			"DESC": "desc"
		},
		configData = new ConfigData(),
		colWidthMap = [];//stores the column width.
		cellRendererMap = []//stores the cell renderer mapping
		sortBy={
			"colIndex": "amount",
			"sortOrder": SORT_ORDER.DESC
		};
	/*
	* Creates table header
	*/
	function createTableHeader(){
		var $snTableHeaderRow = $(".sn-table-header-container .sn-table .sn-table-row").empty(),
			colIndex="",
			deleteColFromModel;
		configData.headerConfig.columns.map(function(item, index){
			var colWidth = item["width"],
				style='min-width:' + colWidth + ';max-width:' + colWidth + ';width:' + colWidth;
			colWidthMap[item["index"]] = colWidth;
			cellRendererMap[item["index"]] = item["cellRenderer"];
			$snTableHeaderRow.append("<div style='" + style + "' data-col-index=" + item["index"] + " class='sn-table-header sn-col-index-" + item["index"] + "'><div title='Click to sort the column' class='sn-col-arrow sn-col-up-arrow-n'></div><span class='sn-header-label'>" + item["label"] + "</span><div title='Click to delete the column' class='sn-delete-col'></div></div>");
		});
		//
		deleteColFromModel = function(colIndexToDelete){
			//delete col from data
			configData.tableData.map(function(item, index){
				for(var col in item){
					if(col === colIndexToDelete){
						delete item[col];	
					};		
				};
			});
			//delete col from headerconfig
			configData.headerConfig.columns.map(function(item, index){
				if(item["index"] === colIndexToDelete){
					delete configData.headerConfig.columns[index];
				}
			});
			//
		};
		//
		$snTableHeaderRow.find(".sn-delete-col").on("click", function(event){
			colIndex = $(event.target).closest(".sn-table-header").data("colIndex");
		    var strconfirm = confirm("Are you sure you want to delete the column ?");
		    if (strconfirm == true) {
				//delete data from the data modal
				deleteColFromModel(colIndex);
				//after deleting rebuild the table header
				createTableHeader();
				//and then render the table
				renderTable(sortBy["colIndex"], sortBy["sortOrder"]);
				//validation for not to delete single column
				if($(".sn-table-row .sn-table-header").length === 1){
					$snTableHeaderRow.find(".sn-delete-col").off("click");//release the memory
					$snTableHeaderRow.find(".sn-delete-col").removeClass("sn-delete-col")
															.addClass("sn-delete-col-ban")
															.attr("title", "Atleast one column must exists");
				};
				//
		    };
		});
		//
		$snTableHeaderRow.find(".sn-header-label").on("click", function(event){	
			colIndex = $(event.target).closest(".sn-table-header").data("colIndex");
			if(sortBy["colIndex"] === colIndex){
				sortBy["sortOrder"] = sortBy["sortOrder"] === SORT_ORDER.DESC ? SORT_ORDER.ASC : SORT_ORDER.DESC;
			} else {
				sortBy["colIndex"] = colIndex;
				sortBy["sortOrder"] = SORT_ORDER.DESC;
			}
			renderTable(sortBy["colIndex"], sortBy["sortOrder"]);
		});
	};
	/**
	Create table body
	*/
	function createTableBody(){
		var $snTableBody = $(".sn-table-body-container .sn-table").empty();
		configData.tableData.map(function(item, index){
			var $snTableRow = $snTableBody.append("<div class='sn-table-row sn-table-row-" + index + "'></div>");
			for(var colName in item){
				var colWidth = colWidthMap[colName],
					style='min-width:' + colWidth + ';max-width:' + colWidth + ';width:' + colWidth;;//in pixels
				//$snTableRow.find(".sn-table-row-" + index).append("<div style='min-width:" + colWidth + "' class='sn-table-cell sn-col-index-" + colName + "'><span>" + item[colName] + "<span/></div>");
				$snTableRow.find(".sn-table-row-" + index).append("<div style='" + style + "' class='sn-table-cell sn-col-index-" + colName + "'></div>");
				var cellContainer = $snTableRow.find(".sn-table-row-" + index + " .sn-col-index-" + colName);
				if(cellRendererMap[colName]){
					var html = cellRendererMap[colName].apply(undefined, [item[colName]]);
					cellContainer.append(html);
				}else{//if there is no cellRendererConfigured by the developer, fallback to default <span/>
					cellContainer.append("<span>" + item[colName] + "</span>");
				};
			};
		});
	};
	/**
	*show / hide the sort arrow based on the colIndex and sort order
	*/
	function showHideSortArrow(){
		$(".sn-table-header .sn-col-arrow").removeClass("sn-col-down-arrow-n");
		$(".sn-table-header .sn-col-arrow").removeClass("sn-col-down-arrow-y");
		$(".sn-table-header .sn-col-arrow").removeClass("sn-col-up-arrow-n");
		$(".sn-table-header .sn-col-arrow").removeClass("sn-col-up-arrow-y");
		//
		if(sortBy["sortOrder"] === SORT_ORDER.DESC){
			$(".sn-table-header.sn-col-index-" + sortBy["colIndex"] + " div").first().addClass("sn-col-down-arrow-y");
		}else if(sortBy["sortOrder"] === SORT_ORDER.ASC){
			$(".sn-table-header.sn-col-index-" + sortBy["colIndex"] + " div").first().addClass("sn-col-up-arrow-y");
		};
		$(".sn-table-body-container .sn-table-row .sn-col-index-" + sortBy["colIndex"]).css("background-color", "#ccc");
	};
	//
	/**
	set the table body container size
	*/
	function setBodyContainerSize(){
		$(".sn-table-body-container").height($(".sn-inner-container").height() - $(".sn-table-header-container").height());
	};
	/**
	listen for scroll event and move the header accordingly by setting the offset
	*/
	function listenForHorizontalScrolling(){
	    $(".sn-table-body-container").scroll(function (){
	        $(".sn-table-header-container").offset({ left: (-1 * (this.scrollLeft)) + 8 });
	    });
	    setBodyContainerSize();
	};
	/*
	sort the data based on the column index, when clicked on header label.
	@colIndex - index of the column to sort - index must be unique to identify a column
	@sortOrder - desc or asc
	*/
	function sort(colIndex, sortOrder){
		configData.tableData.sort(function(objectA, objectB){
			switch(typeof objectA[colIndex]){
				case "string"://sorting a string datatype is case insensitive
					var returnValue=0;
					//
					if(sortOrder == SORT_ORDER.DESC){
						if(objectA[colIndex].toUpperCase() > objectB[colIndex].toUpperCase()){
							returnValue = -1;
						}else{
							returnValue = 1;
						};
					}else{
						if(objectA[colIndex].toUpperCase() < objectB[colIndex].toUpperCase()){
							returnValue = -1;
						}else{
							returnValue = 1;
						};
					};
					//
					return returnValue;
					break;
				case "number":
					return sortOrder == SORT_ORDER.DESC ? objectB[colIndex] - objectA[colIndex] : objectA[colIndex] - objectB[colIndex];
					break;
				case "boolean":
					var returnValue = 0;
					if(sortOrder === SORT_ORDER.DESC){
						returnValue = (objectB[colIndex] === objectA[colIndex])? 0 : objectB[colIndex]? -1 : 1;
					}else{
						returnValue = (objectB[colIndex] === objectA[colIndex])? 0 : objectB[colIndex]? 1 : -1;
					};
					return returnValue;
					break;
			};	
		});
	};
	/*
	Render sorted table
	@colIndex - index of the column to sort - index must be unique to identify a column
	@sortOrder - desc or asc
	*/
	function renderTable(colIndex, sortOrder){
		sort(colIndex, sortOrder);
		createTableBody();
		showHideSortArrow();
		listenForHorizontalScrolling();	
	};
	//
	createTableHeader();
	renderTable(sortBy["colIndex"], sortBy["sortOrder"]);
	//
});
Running the ServiceNow Table application
========================================

Below steps explains how to run the servicenow table application locally.

Steps to run the servicenow table application locally using git
---------------------------------------------------------------

> - Install git in your machine.
> - Assuming you have a directory called codebase in your machine, go to terminal and $cd /codebase. Now you are inside codebase directory.
> - $git clone https://gitlab.com/dharm.arum/servicenow-table.git
> - $cd servicenow-table
> - You should see the following folders/files - data/ img/ libs/ app.js index.html and README.md
> - Now open index.html in any web browser


Steps to run the servicenow table application locally using zip file
-----------------------------------------------------
> - Download the servicenow-table.zip by clicking 'Download zip' menu from https://gitlab.com/dharm.arum/servicenow-table/tree/master.
> - Unzip the servicenow-table.zip anywhere in your machine. 
> - $cd servicenow-table
> - Open index.html in any web browser

Features supported by the table
-------------------------------
> - Supports dynamically generated columns and data sets.
> - Scroll the table horizontally and veritically with fixed header.
> - Column header and rows respects long text content. Word wraps to next line, in case content is big enough to fit.
> - Delete icon is placed to the right of the header label. Clicking on the delete icon, deletes the column, except the last one.
> - By default table is sorted using Amount column by descending order. Clicking on any other header label, sorts the column default by descending order. Clicking it again will sort in ascending order.
> - User confirmation needed before deleting the column.
> - Up/Down arrow in the column header would indicate the sorting order.
> - Developer can configure cellRenderer in column configuration.
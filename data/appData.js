/**
Config data to config table data and header columns
*/
function ConfigData(){
	var data = {
		"headerConfig":{
			"columns":[{
				"label": "Type",
				"index": "type",
				"width": "80px",
				"cellRenderer": function(cellValue){
					//take the cellValue and return back a valid html,
					//based on the cellValue we can have a logic to return different html
					var returnValue = "Unknown";
					switch (cellValue){
						case "CC":
						returnValue = "Credit Card";
						break;
						case "POS":
						returnValue = "Point of Sale";
						break;
						default:
						returnValue = "Unknown";
					}
					return returnValue;
				}
			}, {
				"label": "Short Description",
				"index": "extdesc",
				"width": "100px",
				"cellRenderer": function(cellValue){
					//take the cellValue and return back a valid html,
					//based on the cellValue we can have a logic to return different html
					return '<a href="http://www.amazon.com">' + cellValue + '</a>';//should be a valid html
				}
			}, {
				"label": "Long Description",
				"index": "description",
				"width": "300px"
			}, {
				"label": "Amount",
				"index": "amount",
				"width": "100px"
			}, {
				"label": "Differed",
				"index": "differed",
				"width": "100px",
				"cellRenderer": function(cellValue){
					return cellValue === true? "Yes" : "No";
				}
			}, {
				"label": "Date",
				"index": "date",
				"width": "100px"
			}]
		},
		"tableData": [{
			type: 'POS',
			extdesc: 'ABC VONS Store 1797 SAN DIEGO CAUS',
			description: 'POS Transaction VONS Store 1797 SAN DIEGO CAUS',
			amount: -4.43,
			differed: false,
			date: '2016-02-23'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 6,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: -76.99,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: -176.99,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 76.99,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 9,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: -76.99,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 11,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 20,
			differed: true,
			date: '2016-04-08'
		}, {
			type: 'CC',
			extdesc: 'Amazon.com',
			description: 'online retailer Amazon.com',
			amount: 21,
			differed: true,
			date: '2016-04-08'
		}]
	};
	return data;
};